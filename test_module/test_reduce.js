
const { reduce} = require("../main_module/reduce");

console.log("\nTesting 'reduce' function:");
const sum = reduce([1, 2, 3], (accumulator, current) => accumulator + current, 0);
console.log("Sum:", sum);
