const { find } = require("../main_module/find");

console.log("\nTesting 'find' function:");
const foundElement = find([1, 2, 3], element => element === 2);
console.log("Found Element:", foundElement);

