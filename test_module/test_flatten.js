const { flatten} = require("../main_module/flatten");


console.log("\nTesting 'flatten' function:");
const flattenedArray = flatten([1, [2], [3, [[4]]]]);
console.log("Flattened Array:", flattenedArray);