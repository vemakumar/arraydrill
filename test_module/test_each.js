const { each } = require("../main_module/each");

console.log("Testing 'each' function:");
each([1, 2, 3], (element, index) => {
    console.log(`Element at index ${index}: ${element}`);
});