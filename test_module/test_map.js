const { map} = require("../main_module/map");

console.log("\nTesting 'map' function:");
const mappedArray = map([1, 2, 3], element => element * 2);
console.log("Mapped Array:", mappedArray);
