const { filter} = require("../main_module/filter");

console.log("\nTesting 'filter' function:");
const filteredArray = filter([1, 2, 3], element => element > 1);
console.log("Filtered Array:", filteredArray);